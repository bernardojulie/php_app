<?php 

//session_start();

//Request DB connection
require './lib/connect.php';

function getTitle() {
	echo "Add New Employee";
}	

include "./partials/head.php";

 ?>

</head>
<body>

	<?php include "./partials/header.php"; ?>

	<main class="edit_emp_wrapper">
		<h1 class="text-center">Update Employee Record</h1>

		<div class="container">
			<div class="row">
				<div class="col-md-8 offset-md-2">

					<?php 

					if (isset($_GET['id'])) {
						$emp_id = $_GET['id'];

						$sql = "SELECT * FROM employees WHERE id='$emp_id'";
						$result_qry = mysqli_query($conn, $sql);
					}

					foreach ($result_qry as $records) {
						extract($records);
					}

					 ?>

					<form action="./lib/update_employee.php" method="POST">
						<input type="hidden" name="id" value=<?php echo "$id"; ?>>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text">Name</span>
							</div>
							<input type="text" class="form-control" name="firstName" value=<?php echo "'$first_name'"; ?> required>
							<input type="text" class="form-control" name="lastName" value=<?php echo "'$last_name'"; ?> required>
						</div>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text">Date of Birth</span>
							</div>
							<input type="date" class="form-control" id="datepicker" name="dateOfBirth" value=<?php echo "$date_of_birth"; ?> required>
						</div>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text">Gender</span>
							</div>
							<div class="form-control">
								<label class="form-check-label">
									<input name="gender" type="radio" <?php if($gender=="Male"){echo "checked";} ?> value="Male" required>Male
								</label>
								<label class="form-check-label">
									<input name="gender" type="radio" <?php if($gender=="Female"){echo "checked";} ?> value="Female" required>Female
								</label>
							</div>
						</div>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text">Job Title</span>
							</div>
							<input type="text" class="form-control" name="jobTitle" value=<?php echo "'$job_title'"; ?> required>
						</div>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text">Date Employed</span>
							</div>
							<input type="date" class="form-control" id="datepicker2" name="dateEmployed" value=<?php echo "$date_employed"; ?> required>
						</div>
						<div class="btn-update">
							<button name="updateEmployee" type="submit" class="btn btn-outline-primary">Update</button>
							<a href="./employees_all.php" class="btn btn-outline-danger">Cancel</a>
						</div>
					</form>
				</div>
			</div>
		</div>
		
	</main>

	<!-- <?php include "./partials/footer.php"; ?> -->

<?php include "./partials/foot.php"; ?>