<?php 

//session_start();

//Request DB connection
require './lib/connect.php';

function getTitle() {
	echo "Add New Employee";
}	

include "./partials/head.php";

 ?>

</head>
<body>

	<?php include "./partials/header.php"; ?>

	<main class="add_emp_wrapper">
		<h1 class="text-center">Register New Employee</h1>

		<div class="container">
			<div class="row">
				<div class="col-md-8 offset-md-2">
					<form action="./lib/create_employee.php" method="POST">
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text">Name</span>
							</div>
							<input type="text" class="form-control" name="firstName" placeholder="First Name" required>
							<input type="text" class="form-control" name="lastName" placeholder="Last Name" required>
						</div>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text">Date of Birth</span>
							</div>
							<input type="date" class="form-control" id="datepicker" name="dateOfBirth" placeholder="dd-mm-yyyy" required>
						</div>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text">Gender</span>
							</div>
							<div class="form-control">
								<label class="form-check-label">
									<input name="gender" type="radio" value="male" required>Male
								</label>
								<label class="form-check-label">
									<input name="gender" type="radio" value="female" required>Female
								</label>
							</div>
						</div>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text">Job Title</span>
							</div>
							<input type="text" class="form-control" name="jobTitle" required>
						</div>
						<div class="input-group">
							<div class="input-group-prepend">
								<span class="input-group-text">Date Employed</span>
							</div>
							<input type="date" class="form-control" id="datepicker2" name="dateEmployed" placeholder="dd-mm-yyyy" required>
						</div>
						<div class="btn-register">
							<button name="addEmployee" type="submit" class="btn btn-outline-primary">Register</button>
						</div>
					</form>
				</div>
			</div>
		</div>
		
	</main>

	<!-- <?php include "./partials/footer.php"; ?> -->

<?php include "./partials/foot.php"; ?>