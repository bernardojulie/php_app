-- phpMyAdmin SQL Dump
-- version 4.8.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 22, 2019 at 04:16 AM
-- Server version: 10.1.33-MariaDB
-- PHP Version: 7.2.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `bittel_employees`
--

-- --------------------------------------------------------

--
-- Table structure for table `employees`
--

CREATE TABLE `employees` (
  `id` int(11) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `date_of_birth` date NOT NULL,
  `gender` varchar(6) NOT NULL,
  `job_title` varchar(30) NOT NULL,
  `date_employed` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `employees`
--

INSERT INTO `employees` (`id`, `first_name`, `last_name`, `date_of_birth`, `gender`, `job_title`, `date_employed`) VALUES
(1, 'Julie', 'Bernardo', '1982-07-01', 'Female', 'Web Developer', '2019-01-22'),
(2, 'Mitch', 'Cenizal', '1980-02-14', 'Female', 'Business Analyst', '2019-01-14'),
(5, 'Chris', 'Panters', '2009-03-01', 'Male', 'Software Tester', '2018-12-07'),
(6, 'Ma. Richelle', 'Aquino', '1977-10-01', 'Female', 'Hr Manager', '2012-08-17'),
(7, 'Richard', 'Antel', '1970-02-04', 'Male', 'Chief Financial Officer', '2010-07-15'),
(8, 'Billy', 'Cruz', '1970-04-09', 'Male', 'Chief Operating Officer', '2010-07-02'),
(9, 'John Adolfo', 'Acosta', '1995-11-02', 'Male', 'Web Developer', '2018-02-09'),
(10, 'Jane', 'Victore', '1982-12-22', 'Female', 'Senior Web Developer', '2010-12-10'),
(11, 'Sherrie', 'Guibone', '1990-04-04', 'Female', 'Graphic Artist', '2013-05-05'),
(12, 'Daniel', 'Doe', '1970-01-01', 'Male', 'Chief Executive Officer', '2010-01-28'),
(13, 'Alexa', 'Berns', '1995-10-04', 'Female', 'Trainee Programmer', '2019-01-05');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `employees`
--
ALTER TABLE `employees`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
