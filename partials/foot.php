<!-- Imports jQuery -->
<script type="text/javascript" src="../assets/js/jquery.min.js"></script>

<!-- Imports Bootstrap script -->
<script type="text/javascript" src="../assets/css/bootstrap/js/bootstrap.min.js"></script>

<!-- Imports datepicker script -->
<script type="text/javascript" src="../assets/datepicker/js/gijgo.min.js"></script>

<!-- Imports Custom script -->
<script type="text/javascript" src="../assets/js/script.js"></script>

</body>
</html>