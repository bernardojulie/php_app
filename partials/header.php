<nav class="navbar navbar-logo">
  <!-- Brand Logo -->
  <img src="../assets/img/logo2.png">
</nav>  <!-- /.navbar-logo -->

<nav class="navbar navbar-expand-sm navbar-main">
  <button class="navbar-toggler btn btn-outline-secondary" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
      <span class="fas fa-bars"></span>
    </button>
  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    
    <!-- Links -->
    <ul class="navbar-nav">
      <!-- Dropdown -->
      <li class="nav-item dropdown">
        <a class="nav-link dropdown-toggle" href="./employees_all.php" id="navbardrop" data-toggle="dropdown">
          Employees
        </a>
        <div class="dropdown-menu">
          <a class="dropdown-item" href="./employees_all.php">All Employees</a>
          <a class="dropdown-item" href="./add_employee.php">Add new employee</a>
        </div>
      </li>
    </ul>
    <ul class="navbar-nav ml-auto">
      <li>
        <a class="nav-link" href="./lib/logout.php">Logout</a>
      </li>
    </ul>
  </div>
</nav>  <!-- /.navbar-main -->