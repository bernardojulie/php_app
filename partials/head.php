<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="IE=Edge">

	<title>Bittel Asia Employees - <?php getTitle() ?> </title>

	<!-- Imports google fonts -->
	<link href="https://fonts.googleapis.com/css?family=Comfortaa:300" rel="stylesheet">

	<!-- Imports icons -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css" integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/" crossorigin="anonymous">

	<!-- Imports Bootstrap stylesheet -->
	<link rel="stylesheet" type="text/css" href="./assets/css/bootstrap/css/bootstrap.min.css">

	<!-- Imports datepicker -->
	<link rel="stylesheet" type="text/css" href="./assets/datepicker/css/gijgo.min.css">

	<!-- Imports Custom stylesheet -->
	<link rel="stylesheet" type="text/css" href="./assets/css/style.css">
	


