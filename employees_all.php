<?php 

//session_start();

//Request DB connection
require './lib/connect.php';

function getTitle() {
	echo "All Employees";
}	

include "./partials/head.php";

 ?>

</head>
<body>

	<?php include "./partials/header.php"; ?>

	<main class="employees_wrapper">
		<h1 class="text-center">All Employees</h1>
		<table class="table table-hover table-responsive" id="table-all">
			<thead class="thead-col">
				<tr>
					<th>First Name</th>
					<th>Last Name</th>
					<th>Date of Birth</th>
					<th>Gender</th>
					<th>Job Title</th>
					<th>Date Employed</th>
					<th></th>
					<th></th>
				</tr>
			</thead>
			<tbody>
				<?php 

				//Request data from DB
				$sql = "SELECT * FROM employees";
				$result_qry = mysqli_query($conn, $sql);

				//Records per page
				$records_per_page = 10;

				//# of results  in database
				$number_of_records = mysqli_num_rows($result_qry);

				//Total number of pages
				$number_of_pages = ceil($number_of_records/$records_per_page);

				//Current page
				if (!isset($_GET['page'])) {
					$page = 1;
				} else {
					$page = $_GET['page'];
				}

				//Starting LIMIT for current page
				$starting_limit = ($page - 1) * $records_per_page;

				//Request data from DB
				$employees_sql = "SELECT * FROM employees LIMIT " . $starting_limit . "," . $records_per_page;
				$result_qry2 = mysqli_query($conn, $employees_sql);

				foreach ($result_qry2 as $records) {
					extract($records);
					echo '
						<tr>
							<td>'.$first_name.'</td>
							<td>'.$last_name.'</td>
							<td>'.$date_of_birth.'</td>
							<td>'.$gender.'</td>
							<td>'.$job_title.'</td>
							<td>'.$date_employed.'</td>
							<td class="text-right">
								<a href="edit_employee.php?id='.$id.'" class="btn btn-outline-primary">Edit</a>
							</td>
							<td>
								<!-- Delete product -->
								<!-- Trigger -->
								<button class="btn btn-outline-danger" data-toggle="modal" data-target="#deleteModal'.$id.'">Delete</button>
							</td>
							<!-- deleteproductModal -->
							<div id="deleteModal'.$id.'" class="modal fade" role="dialog">
								<div class="modal-dialog">
								<!-- Modal content -->
									<div class="modal-content">
										<div class="modal-header">
											<h3>Delete Item</h3>
										</div>
										<div class="modal-body text-right">
											<p class="text-center">Do you really want to delete this item?</p>
											<form action="./lib/delete_employee.php?id='.$id.'" method="POST">
												<button name="deleteProd" type="submit" class="btn btn-outline-danger">Yes</button>
												<button type="button" class="btn btn-outline-primary" data-dismiss="modal">No</button>
											</form>
										</div>					
									</div>
								</div>
							</div>
						</tr>

					';

				}

				 ?>
			</tbody>
		</table>

		<table class="table table-hover table-responsive" id="table-mobile">
			<thead>
				<tr>
					<th class="text-center">Name</th>
					<th class="text-right">Details</th>
				</tr>
			</thead>
			<tbody>
				<?php 

				//Request data from DB
				$sql = "SELECT * FROM employees";
				$result_qry = mysqli_query($conn, $sql);

				//Records per page
				$records_per_page = 10;

				//# of results  in database
				$number_of_records = mysqli_num_rows($result_qry);

				//Total number of pages
				$number_of_pages = ceil($number_of_records/$records_per_page);

				//Current page
				if (!isset($_GET['page'])) {
					$page = 1;
				} else {
					$page = $_GET['page'];
				}

				//Starting LIMIT for current page
				$starting_limit = ($page - 1) * $records_per_page;

				//Request data from DB
				$employees_sql = "SELECT * FROM employees LIMIT " . $starting_limit . "," . $records_per_page;
				$result_qry2 = mysqli_query($conn, $employees_sql);

				foreach ($result_qry2 as $records) {
					extract($records);
					echo '

						<tr>
							<td class="text-left">'.$first_name.' '.$last_name.'</td>
							
							<td>
								<button type="button" class="btn btn-outline-primary" data-toggle="collapse" data-target="#details'.$id.'">
									<span class="fas fa-bars"></span>
								</button>
								<div id="details'.$id.'" class="collapse">
									<div>
										<p> Date of Birth: '.$date_of_birth.' </p>
										<p> Gender: '.$gender.' </p>
										<p> Job Title: '.$job_title.' </p>
										<p> Date Employed: '.$date_employed.' </p>

										<a href="edit_employee.php?id='.$id.'" class="btn btn-outline-primary btn-edit">Edit</a>

										<!-- Delete product -->
										<!-- Trigger -->
										<button class="btn btn-outline-danger btn-del" data-toggle="modal" data-target="#deleteModal'.$id.'">Delete</button>
									</div>
								
									<!-- deleteproductModal -->
									<div id="deleteModal'.$id.'" class="modal fade" role="dialog">
										<div class="modal-dialog">
										<!-- Modal content -->
											<div class="modal-content">
												<div class="modal-header">
													<h3>Delete Item</h3>
												</div>
												<div class="modal-body text-right">
													<p class="text-center">Do you really want to delete this item?</p>
													<form action="./lib/delete_employee.php?id='.$id.'" method="POST">
														<button name="deleteProd" type="submit" class="btn btn-outline-danger">Yes</button>
														<button type="button" class="btn btn-outline-primary" data-dismiss="modal">No</button>
													</form>
												</div>					
											</div>
										</div>
									</div>
								</div>
							</td>
						</tr>

					';

				}

				 ?>
			</tbody>
		</table>

		<div class="employees_page">
			<ul class="pagination justify-content-end">

				<?php 

				// display the links to the pages
				for ($page=1; $page<=$number_of_pages; $page++) {
				  	echo '
				  		<li class="page-item"><a class="page-link" href="employees_all.php?page=' . $page . '">' . $page . '</a></li>
				  	';
				}

				 ?>

			</ul>
		</div>
	</main>

	<!-- <?php include "./partials/footer.php"; ?> -->

<?php include "./partials/foot.php"; ?>