<?php

//Parameters for DB connection
$hostname = 'localhost';
$username = 'root';
$password = '';
$db_name = 'bittel_employees';

//Connection to DB
$conn = mysqli_connect($hostname, $username, $password, $db_name);

//Test DB connection
// if (!$conn)
// 	die('Connection Failed:' . mysqli_error($conn));
// else
// 	echo 'Connection successful.';